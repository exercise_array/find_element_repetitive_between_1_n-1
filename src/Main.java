import java.util.HashSet;
import java.util.Set;

public class Main {
    /**
     * Find the only repetitive element between 1 to n-1
     * Source: https://www.geeksforgeeks.org/find-repetitive-element-1-n-1/
     */

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 1, 4, 5, 6};
        System.out.println("Element repetitive is: " + findRepeatLoop(arr, arr.length));
        System.out.println("Element repetitive is: " + findRepeatFormula(arr, arr.length));
        System.out.println("Element repetitive is: " + findRepeatHashing(arr, arr.length));
    }

    public static int findRepeatLoop(int[] arr, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (arr[i] == arr[j])
                    return arr[i];
            }
        }
        return -1;
    }

    public static int findRepeatFormula(int[] arr, int n) {
        int sum = 0;
        for(int value : arr) {
            sum += value;
        }
        int formula = n * (n - 1) / 2;
        return sum - formula;
    }

    public static int findRepeatHashing(int[] arr, int n) {
        Set set = new HashSet();
        for(int value : arr) {
            if (set.contains(value))
                return value;
            else set.add(value);
        }
        return -1;
    }
}
